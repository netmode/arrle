## What is ARRLE
ARRLE stands for an Activities Recommender Engine based on Reinforcement learing  
This project models both the Environment simulation as well as the Agent that tries to solve the problem of recommending emotional training activities to a social group based on the emotional profile of the group.  
## Requirements
### ARRLE version 1
RecSim python library (https://github.com/google-research/recsim)  
tensorflow version 1.15  
python version 3  
### ARRLE version 2 (EduRecRL)
OpenAI Gym toolkit (https://gym.openai.com/)  
Stable-Baselines (https://stable-baselines.readthedocs.io/en/master/index.html)  
tensorflow version 1.14  
python version 3 ((>=3.5))  


## Lead Developers

The following lead developers are responsible for this repository and have admin rights. They can, for example, merge pull requests.

    Eleni Fotopoulou (@elfo)
    Anastasios Zafeiropoulos (@tzafeir )
	Ioanna Mandilara (@ioanna.mand96)


## Cite this Work

If you use this tool for your research, publications, or personal projects, please consider to cite the following paper:
```
@inproceedings{arrle,
	Author = {Eleni Fotopoulou, Anastasios Zafeiropoulos, Michalis Feidakis, Dimitrios Metafas, Symeon Papavassiliou},
	Title = {ITS2020 Conference on Intelligent Tutoring Systems},
	Month = {June},
	Pages = {1-10},
	Title = {An Interactive Recommender System based on Reinforcement Learning for Improving Emotional Competences in Educational Groups},
	Year = {2020}}

```
