import numpy as np
import gym
from gym import spaces
import random
import matplotlib.pyplot as plt 
import seaborn as sns
import tensorflow as tf
import os
import sys
sns.set_theme()


def evaluate(envs,model,n_steps = 100,name='A2C',colors='indianred'):
    
    """
    Evaluate a RL agent
    :param model: (BaseRLModel object) the RL Agent
    :param num_steps: (int) number of timesteps to evaluate it
    :return: (float) Mean reward for the last 100 episodes
    """
    # Test the trained agent
    obs = envs.reset()
    rewards,steps = [],[]
    for step in range(n_steps):
        action, _ = model.predict(obs, deterministic=True)
        print("Step {}".format(step + 1))
        print("Action: ", action)
        obs, reward, done,info = envs.step(action)
        print('obs=', obs, 'reward=', reward, 'done=', done,'info=',info)
        envs.render(mode='console')
        rewards.append(reward)
        steps.append(step)
        if done:
            envs.reset()
            print("Goal reached!", "reward=", reward)
            break
    
    # Close the env
    envs.close()
    # Compute mean reward
    mean_100ep_reward = round(np.mean(rewards), 1)
    print("Mean reward:", mean_100ep_reward, "Num episodes:", len(steps))
    cumsum_reward = np.cumsum(rewards)
    
    fig = plt.figure(figsize = (10,10))
    fig.suptitle(name,size=19,x=0.53)
    plt.subplot(2,1, 1)
    plt.plot(steps,rewards,color=colors)
    plt.xlabel('Steps',size=14)
    plt.ylabel('Reward',size =14)
    plt.title('Reward VS Steps',size=16)
    # plt.grid()

    plt.subplot(2,1, 2)
    plt.plot(steps,cumsum_reward,color=colors)
    plt.xlabel('Steps',size = 14)
    plt.ylabel('Cumulative Reward',size = 14)
    plt.title('Cumulative Reward VS Steps',size=16)
    # plt.grid()
    fig.tight_layout()
    plt.show()
    
def testing(env,n_steps=100):
    obs = env.reset()
    env.render()
    rewards = []
    steps = []
    for step in range(n_steps):
      action = env.action_space.sample()
      print(action)
      print("Step {}".format(step + 1))
      obs, reward, done, info = env.step(action)
      print('obs=', obs, 'reward=', reward, 'done=', done,'info',info)
      env.render()
      rewards.append(reward)
      steps.append(step)
      if done:
        print("Goal reached!", "reward=", np.mean(rewards))
        print('Number of steps:',step)
        break
      
    plt.plot(steps,rewards,color='royalblue')
    plt.xlabel('Steps')
    plt.ylabel('Reward')
    plt.title('Reward VS Steps (Random Actions)')
    plt.show()
