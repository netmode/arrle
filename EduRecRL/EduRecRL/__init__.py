from gym.envs.registration import register

register(
    id='EduRecEnv-v0',
    entry_point='EduRecRL.envs:EduRecEnv',
)
