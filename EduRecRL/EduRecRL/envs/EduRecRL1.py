import numpy as np
import gym
from gym import spaces
import random
import matplotlib.pyplot as plt 
import tensorflow as tf

class EduRecEnv(gym.Env):
  """
  Custom Environment that follows gym interface.
  """
  metadata = {'render.modes': ['console']}

  def __init__(self):
    super(EduRecEnv, self).__init__()
    

    # Define action and observation space
    # They must be gym.spaces objects
    
    # Action space
    self.action_space = spaces.MultiBinary(30)
    
    # Observation space
    self.observation_space = spaces.Box(low=-1, high=1,
                                      shape=(30,), dtype=np.float64)
    # State
    np.random.seed(1)
    self._state = np.random.uniform(-1,1,30)
    
    # Time budget (Minutes)
    self._time_budget = 3600
    
    
  def reset(self):

    # Reset state
    np.random.seed(1)
    self._state =np.random.uniform(-1,1,30)
    self._time_budget = 3600
    early_stop = False
    return self._state

  def step(self, action):
    
    # Duration of activity
    self._activity_duration = random.uniform(30.0,120.0)

    # Calculate group's interest given the action
    group_interest = (np.dot(action,self._state))
    
    # Normalize group's interest in [0,1] 
    positive_update_prob = (group_interest+sum(action))/(2*sum(action))
    
    # Bad & Well developed microcompetences
    condition_bd = self._state >= 0
    condition_wd = self._state < 0
    
    # Initialize early stopping
    early_stop = False
    # Count the percentage of well developed microcompetences before the update of the state
    all_comp = (np.count_nonzero(self._state<=0))/30

    # Normalize state in [0,1]
    self._state = (self._state + 1)/2
    
    # High values of positive_update_probability indicate that the the proposed action tackle the group's social and emotional needs 
    # and as a result the micro-competences of the educational group are reduced by 30% (high shift & no penalty).
    if positive_update_prob >= 0.45:
        # only reward
        self._state[np.where((action==1) & (condition_bd)==True)] = self._state[np.where((action==1) & 
                                                                                     (condition_bd)==True)]*0.7
    # Low values of positive_update_probability reflect that the action proposed by the agent is not aimed at the micro-competences of the group 
    # that need to be strengthen and so the well-developed micro-competences are increased by 20% and the bad-developed are reduced by 10%(lower shift).
    elif (positive_update_prob >=0.2) & (positive_update_prob <0.45):
        # reward
        self._state[np.where((action==1) & (condition_bd)==True)] = self._state[np.where((action==1) & 
                                                                                        (condition_bd)==True)]*0.8
        # penalty
        self._state[np.where((action==1) & (condition_wd)==True)] = self._state[np.where((action==1) 
                                                                                        & (condition_wd)==True)]*1.1
    # In this case, the positive_update_probability is vey low and simultaneously the percentage of well developed micro-competences is greater than 80%.
    # Since almost all micro-competences are at the desired level, i.e they do not need further improvement  then it is reasonable 
    # the positive_update_probability of receiving low values and perfoming early stopping
    elif (positive_update_prob <0.2) & (all_comp >0.8):
        early_stop = True
    # If the proposed action does not tackle the group's social and emotional needs and other micro-competences needs further improvement,
    # then they decreased by 20%
    elif (positive_update_prob<0.2):
        self._state[np.where((action==1) & (condition_wd)==True)] = self._state[np.where((action==1) 
                                                                                        & (condition_wd)==True)]*1.2
    # Normalize again the state in [-1,1]
    self._state = 2*(self._state) - 1
    # Limit the values of microcompetences in the range [-1,1]
    self._state = np.clip(self._state,-1.0,1.0)
    

    # If there is at least one microcompetence, that needs imporevement and action does not refer to it, then we count the number of these
    # microcompetences, in order to decrease the reward.
    max_bad_comp = max(self._state[np.where(action==1)]) 
    check = any(i >= max_bad_comp for i in self._state[np.where(action==0)])
    # Count bad developed microcompetences, where action is not applicable.
    bad_dev = (self._state[np.where(action==0)]>= max_bad_comp).sum()

    # Compute the number of microcompetences  with negative values(i.e. well developed)
    well_dev = np.count_nonzero(self._state<=0)    
    if (check == True):
        reward = well_dev/30 - bad_dev/30
    else:
        reward = well_dev/30
    
    # Reduce time budget at each time step
    self._time_budget -= self._activity_duration
    
    # done : A boolean value stating whether it’s time to reset the environment again.
    done = bool((self._time_budget<0) | ((self._time_budget<0) & (well_dev>27)) | 
                ((self._time_budget>0) & (well_dev>27))|(early_stop==True))

    # Optionally we can pass additional info.
    info = {'activity_duration':self._activity_duration,'update prob':positive_update_prob,
            'well dev comp':well_dev,'time budget':self._time_budget,'early_stop':early_stop}

    return self._state, reward, done, info

  def render(self, mode='console'):
    if mode != 'console':
      raise NotImplementedError()
    
  def close(self):
    pass
