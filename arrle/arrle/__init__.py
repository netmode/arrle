from gym.envs.registration import register

register(
    id='arrle-v0',
    entry_point='arrle.envs:long_term_satisfaction',
)

register(
    id='arrle-v1',
    entry_point='arrle.envs:emotional_competences_improvement',
)
