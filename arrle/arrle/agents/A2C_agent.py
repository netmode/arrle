
import gym
import numpy as np
import matplotlib.pyplot as plt
#import tensorflow as tf

from arrle.envs import emotional_competences_improvement


from stable_baselines.common.policies import MlpPolicy
from stable_baselines.common import make_vec_env
from stable_baselines import A2C

env_config = {}
env_config['slate_size'] = 2
env_config['num_candidates'] = 10
env_config['resample_documents']=False
env_config['seed']=0
env=emotional_competences_improvement.create_environment(env_config)
env.reset()

model = A2C(MlpPolicy, env, verbose=1)
model.learn(total_timesteps=25000)
model.save("a2c_emotion_completences_improvement")

del model # remove to demonstrate saving and loading

model = A2C.load("a2c_emotion_completences_improvement")

obs = env.reset()
while True:
    action, _states = model.predict(obs)
    obs, rewards, dones, info = env.step(action)
    env.render()
