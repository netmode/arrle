--------------------------------------------------------------------------
instantiate environment long_term_satisfaction
--------------------------------------------------------------------------
from recsim.simulator import recsim_gym
#from recsim.environments import long_term_satisfaction
from arrle.envs import long_term_satisfaction

env_config = {}
env_config['slate_size'] = 3
env_config['num_candidates'] = 10
env_config['resample_documents']=True
ltsenv = long_term_satisfaction.create_environment(env_config)

observation_0 = ltsenv.reset()

print('Observation 0')
print('Available documents')
doc_strings = ['doc_id ' + key + " kaleness " + str(value) for key, value in observation_0['doc'].items()]
print('\n'.join(doc_strings))
print('Noisy user state observation')
print(observation_0['user'])
# Agent recommends the first three documents.
recommendation_slate_0 = [0, 1, 2]
observation_1, reward, done, _ = ltsenv.step(recommendation_slate_0)
print('Observation 1')
print('Available documents')
doc_strings = ['doc_id ' + key + " kaleness " + str(value) for key, value in observation_1['doc'].items()]
print('\n'.join(doc_strings))
rsp_strings = [str(response) for response in observation_1['response']]
print('User responses to documents in the slate')
print('\n'.join(rsp_strings))
print('Noisy user state observation')
print(observation_1['user'])

--------------------------------------------------------------------------
instantiate environment interest_evolution
--------------------------------------------------------------------------
from recsim.simulator import recsim_gym
#from recsim.environments import interest_evolution
from arrle.envs import interest_evolution


env_config = {}
env_config['slate_size'] = 3
env_config['num_candidates'] = 10
env_config['resample_documents']=True
env_config['seed']=12
ltsenv = interest_evolution.create_environment(env_config)

observation_0 = ltsenv.reset()
print('Observation 0')
print('Available documents')
doc_strings = ['doc_id ' + key + " features " + str(value) for key, value in observation_0['doc'].items()]
print('\n'.join(doc_strings))
print('Noisy user state observation')
print(observation_0['user'])
# Agent recommends the first three documents.
recommendation_slate_0 = [0, 1, 2]
observation_1, reward, done, _ = ltsenv.step(recommendation_slate_0)
print('Observation 1')
print('Available documents')
doc_strings = ['doc_id ' + key + " features " + str(value) for key, value in observation_1['doc'].items()]
print('\n'.join(doc_strings))
rsp_strings = [str(response) for response in observation_1['response']]
print('User responses to documents in the slate')
print('\n'.join(rsp_strings))
print('Noisy user state observation')
print(observation_1['user'])
--------------------------------------------------------------------------
instantiate environment emotional_competences_improvement
--------------------------------------------------------------------------
from recsim.simulator import recsim_gym
from arrle.envs import emotional_competences_improvement_multinomial_true_utilitysampler_NEW_with_replaced_flip


env_config = {}
env_config['slate_size'] = 3
env_config['num_candidates'] = 10
env_config['resample_documents']=True
env_config['seed']=0
ltsenv = emotional_competences_improvement_multinomial_true_utilitysampler_NEW_with_replaced_flip.create_environment(env_config)

observation_0 = ltsenv.reset()
print('Observation 0')
print('Available documents')
doc_strings = ['doc_id ' + key + " features " + str(value) for key, value in observation_0['doc'].items()]
print('\n'.join(doc_strings))
print('Noisy user state observation')
print(observation_0['user'])
# Agent recommends the first three documents.
recommendation_slate_0 = [0, 1, 2]
observation_1, reward, done, _ = ltsenv.step(recommendation_slate_0)
print('Observation 1')
print('Available documents')
doc_strings = ['doc_id ' + key + " features " + str(value) for key, value in observation_1['doc'].items()]
print('\n'.join(doc_strings))
rsp_strings = [str(response) for response in observation_1['response']]
print('User responses to documents in the slate')
print('\n'.join(rsp_strings))
print('Noisy user state observation')
print(observation_1['user'])
--------------------------------------------------------------------------
#instantiate agent interest_evolution with FullSlateQAgent
--------------------------------------------------------------------------
import numpy as np
import tensorflow as tf

from recsim.environments import interest_evolution
from recsim.agents import full_slate_q_agent
from recsim.simulator import runner_lib

def create_agent(sess, environment, eval_mode, summary_writer=None):
  kwargs = {
      'observation_space': environment.observation_space,
      'action_space': environment.action_space,
      'summary_writer': summary_writer,
      'eval_mode': eval_mode,
  }
  return full_slate_q_agent.FullSlateQAgent(sess, **kwargs)

seed = 10
np.random.seed(seed)
env_config = {
  'num_candidates': 10,
  'slate_size': 2,
  'resample_documents': True,
  'seed': seed,
  }

tmp_base_dir = '/tmp/recsim/'
runner = runner_lib.TrainRunner(
    base_dir=tmp_base_dir,
    create_agent_fn=create_agent,
    env=interest_evolution.create_environment(env_config),
    episode_log_file="",
    max_training_steps=10,
    num_iterations=10)
runner.run_experiment()

runner = runner_lib.EvalRunner(
      base_dir=tmp_base_dir,
      create_agent_fn=create_agent,
      env=interest_evolution.create_environment(env_config),
      max_eval_episodes=5,
      test_mode=True)
runner.run_experiment()

tensorboard --logdir=/tmp/recsim/
--------------------------------------------------------------------------
#instantiate agent interest_evolution with RandomAgent
--------------------------------------------------------------------------
import numpy as np
import tensorflow as tf

from recsim.environments import interest_evolution
from recsim.agents import random_agent
from recsim.simulator import runner_lib

def create_agent(sess, environment, eval_mode, summary_writer=None):
    kwargs = {
    #'observation_space': environment.observation_space,
    'action_space': environment.action_space
    }
    return random_agent.RandomAgent(**kwargs)


seed = 10
env_config = {
  'num_candidates': 10,
  'slate_size': 2,
  'resample_documents': True,
  'seed': seed,
  }

tmp_base_dir = '/tmp/recsim/'
runner = runner_lib.TrainRunner(
    base_dir=tmp_base_dir,
    create_agent_fn=create_agent,
    env=interest_evolution.create_environment(env_config),
    episode_log_file="",
    max_training_steps=10,
    num_iterations=10)
runner.run_experiment()

runner = runner_lib.EvalRunner(
      base_dir=tmp_base_dir,
      create_agent_fn=create_agent,
      env=interest_evolution.create_environment(env_config),
      max_eval_episodes=5,
      test_mode=True)
runner.run_experiment()

tensorboard --logdir=/tmp/recsim/
--------------------------------------------------------------------------

python3 main.py --logtostderr \
  --base_dir="/tmp/recsim/interest_exploration_full_slate_q" \
  --agent_name=full_slate_q \
  --environment_name=interest_exploration \
  --episode_log_file='episode_logs.tfrecord' \
  --gin_bindings=simulator.runner_lib.Runner.max_steps_per_episode=100 \
  --gin_bindings=simulator.runner_lib.TrainRunner.num_iterations=10 \
  --gin_bindings=simulator.runner_lib.TrainRunner.max_training_steps=100 \
  --gin_bindings=simulator.runner_lib.EvalRunner.max_eval_episodes=5

--------------------------------------------------------------------------
#instantiate agent emotional_competences_improvement with FullSlateQAgent
--------------------------------------------------------------------------
import numpy as np
import tensorflow as tf

from arrle.envs import emotional_competences_improvement_multinomial_true_utilitysampler
from recsim.agents import full_slate_q_agent
from recsim.simulator import runner_lib

def create_agent(sess, environment, eval_mode, summary_writer=None):
  kwargs = {
      'observation_space': environment.observation_space,
      'action_space': environment.action_space,
      'summary_writer': summary_writer,
      'eval_mode': eval_mode,
  }
  return full_slate_q_agent.FullSlateQAgent(sess, **kwargs)

seed = 10
np.random.seed(seed)
env_config = {
  'num_candidates': 5,
  'slate_size': 2,
  'resample_documents': True,
  'seed': seed,
  }

tmp_base_dir = '/tmp/recsim/ECI_multinomial_true_utilitySampler/'
runner = runner_lib.TrainRunner(
    base_dir=tmp_base_dir,
    create_agent_fn=create_agent,
    env=emotional_competences_improvement_multinomial_true_utilitysampler.create_environment(env_config),
    episode_log_file="",
    max_training_steps=1, #60
    num_iterations=1) #100
runner.run_experiment()

runner = runner_lib.EvalRunner(
      base_dir=tmp_base_dir,
      create_agent_fn=create_agent,
      env=emotional_competences_improvement.create_environment(env_config),
      max_eval_episodes=30,
      test_mode=True)
runner.run_experiment()


tensorboard --logdir=/tmp/recsim/ECI_multinomial_true_utilitySampler/
--------------------------------------------------------------------------
#instantiate agent emotional_competences_improvement with randomAgent
--------------------------------------------------------------------------
import numpy as np
import tensorflow as tf

from arrle.envs import emotional_competences_improvement
from recsim.agents import random_agent
from recsim.simulator import runner_lib


def create_agent(sess, environment, eval_mode, summary_writer=None):
    kwargs = {
    #'observation_space': environment.observation_space,
    'action_space': environment.action_space
    }
    return random_agent.RandomAgent(**kwargs)


env_config = {}
env_config['slate_size'] = 2
env_config['num_candidates'] = 10
env_config['resample_documents']=False
env_config['seed']=0


tmp_base_dir = '/tmp/recsim/'
runner = runner_lib.TrainRunner(
    base_dir=tmp_base_dir,
    create_agent_fn=create_agent,
    env=emotional_competences_improvement.create_environment(env_config),
    episode_log_file="",
    max_training_steps=10,
    num_iterations=10)
runner.run_experiment()

runner = runner_lib.EvalRunner(
      base_dir=tmp_base_dir,
      create_agent_fn=create_agent,
      env=emotional_competences_improvement.create_environment(env_config),
      max_eval_episodes=5,
      test_mode=True)
runner.run_experiment()

tensorboard --logdir=/tmp/recsim/
ssh -NfL localhost:6006:10.0.2.112:6006 ubuntu@efotopoulou

-------------------------------------------------------
python3 main.py --logtostderr \
  --base_dir="/tmp/recsim/interest_exploration_full_slate_q" \
  --agent_name=full_slate_q \
  --environment_name=interest_exploration \
  --episode_log_file='episode_logs.tfrecord' \
  --gin_bindings=simulator.runner_lib.Runner.max_steps_per_episode=100 \
  --gin_bindings=simulator.runner_lib.TrainRunner.num_iterations=300 \
  --gin_bindings=simulator.runner_lib.TrainRunner.max_training_steps=100 \
  --gin_bindings=simulator.runner_lib.EvalRunner.max_eval_episodes=100
