from recsim.simulator import recsim_gym
from arrle.envs import emosociocialCompetencesEnv


env_config = {}
env_config['slate_size'] = 3
env_config['num_candidates'] = 10
env_config['resample_documents']=False
env_config['seed']=0
ltsenv = emosociocialCompetencesEnv.create_environment(env_config)

observation_0 = ltsenv.reset()

##add new Activities
#observation_0['doc']['200'] = np.array([1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0])


print('Observation 0')
print('Available documents')
doc_strings = ['doc_id ' + key + " features " + str(value) for key, value in observation_0['doc'].items()]
print('\n'.join(doc_strings))
print('Noisy user state observation')
print(observation_0['user'])
# Agent recommends the first three documents.
recommendation_slate_0 = [0, 1, 2]
observation_1, reward, done, _ = ltsenv.step(recommendation_slate_0)
print('Observation 1')
print('Available documents')
doc_strings = ['doc_id ' + key + " features " + str(value) for key, value in observation_1['doc'].items()]
print('\n'.join(doc_strings))
rsp_strings = [str(response) for response in observation_1['response']]
print('User responses to documents in the slate')
print('\n'.join(rsp_strings))
print('Noisy user state observation')
print(observation_1['user'])
