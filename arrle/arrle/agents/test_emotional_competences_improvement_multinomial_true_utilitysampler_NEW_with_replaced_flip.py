import numpy as np
import tensorflow as tf

from arrle.envs import emotional_competences_improvement_multinomial_true_utilitysampler_NEW_with_replaced_flip
from recsim.agents import full_slate_q_agent
from recsim.simulator import runner_lib

def create_agent(sess, environment, eval_mode, summary_writer=None):
  kwargs = {
      'observation_space': environment.observation_space,
      'action_space': environment.action_space,
      'summary_writer': summary_writer,
      'eval_mode': eval_mode,
  }
  return full_slate_q_agent.FullSlateQAgent(sess, **kwargs)

seed = 10
np.random.seed(seed)
env_config = {
  'num_candidates': 5,
  'slate_size': 2,
  'resample_documents': True,
  'seed': seed,
  }

tmp_base_dir = '/tmp/recsim/ECI_multinomial_true_utilitySampler_NEW_with_replaced_flip/'
num_iterations =100
while num_iterations < 10001:
  print("num_iterations")
  print(num_iterations)
  runner = runner_lib.TrainRunner(
      base_dir=tmp_base_dir,
      create_agent_fn=create_agent,
      env=emotional_competences_improvement_multinomial_true_utilitysampler_NEW_with_replaced_flip.create_environment(env_config),
      episode_log_file="",
      max_training_steps=60, #60
      num_iterations=num_iterations) #100
  runner.run_experiment()
  num_iterations += 100


runner = runner_lib.EvalRunner(
      base_dir=tmp_base_dir,
      create_agent_fn=create_agent,
      env=emotional_competences_improvement_multinomial_true_utilitysampler_NEW_with_replaced_flip.create_environment(env_config),
      max_eval_episodes=30,
      test_mode=True)
runner.run_experiment()


tensorboard --logdir=/tmp/recsim/ECI_multinomial_true_utilitySampler_NEW_with_replaced_flip/
